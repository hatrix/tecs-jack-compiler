import os, sys
import re

class JackTokenizer:
    def __init__(self, files):
        self.files = files
        self.actual = self.files[0]
        
        
        self.setFile()
              
        
    def setFile(self):
        self.current = 0
        self.content = open(self.actual).readlines()
        self.clean()        

    def clean(self):
        # strip comments
        expr = "(.*)//"
        for i in range(len(self.content)):
            if re.search(expr, self.content[i]): # "stuff // comm" -> "stuff"
                self.content[i] = re.search(expr, self.content[i]).group(1)
            
            elif re.search("/\*\*", self.content[i]): # "/**" => ""
                self.content[i] = ""
                
            elif re.match(" ?\*", self.content[i]):
                self.content[i] = ""
            
            elif re.match(" ?\*/", self.content[i]):
                self.content[i] = ""
                
        # split tokens
        pattern = "([\. \(\){},;\+\-\*=~\[\]]<>\|\&)"
        for i in range(len(self.content)):
            self.content[i] = re.split(pattern, self.content[i])
            
        # delete empty entries
        self.content = [w2.strip() for w in self.content
                        for w2 in w if w2.strip()]
            
        # and regroup strings with spaces that have been split
        self.regroup_str()
        
    def regroup_str(self):
    
        start = 0
        i = 0
        
        while i < len(self.content):
            if self.content[i][0] == "\"" and len(self.content[i]) > 1:
                start = 1
            
            elif start:
                if self.content[i][0] == "\"":
                    self.content[i-1] += "\""
                    start = 0
                    
                else:
                    self.content[i-1] += " " + self.content[i]
                
                self.content.remove(self.content[i])
                i -= 1
                
            i += 1
            
    def hasMoreTokens(self):
        return 1 if self.current < len(self.content) else 0
        
    def advance(self):
        self.current += 1
        
    def tokenType(self):
        token = self.content[self.current]
    
        keywords = ["class", "constructor", "function", "method", "field",
                    "static", "var", "int", "char", "boolean", "void", "true",
                    "false", "null", "this", "let", "do", "if", "else", "while",
                    "return"]
        symbols = ["{", "}", "(", ")", "[", "]", ".", ",", ";", "+", "-", "*",
                   "/", "&", "|", "<", ">", "=", "~", "&", "|", "<", ">"]
                   
        if token in keywords:
            return "KEYWORD"
        
        elif token in symbols:
            return "SYMBOL"
            
        elif token[0] == "\"" and token[-1] == "\"":
            return "STRING_CONST"
            
        elif token.isdigit():
            if 0 <= int(token) <= 32767:            
                return "INT_CONST"
            
            else:
                print("value error")
                exit()
        
        elif re.match("^[A-Za-z0-9_-]*$", token):
            return "IDENTIFIER"
            
        else:
            print("Unknown token")
            exit()
            
    def keyWord(self):
        # let's be dirty: since we return the current keyword which is the
        # current token, just set it to uppercase.
        # doesn't handle wrong keywords.
        return self.content[self.current].upper()
                   
    def symbol(self):
        # again
        return self.content[self.current]
        
    def identifier(self):
        # and again
        return self.content[self.current]
    
    def intVal(self):
        # love it
        return self.content[self.current]
    
    def stringVal(self):
        # cool
        return self.content[self.current][1:-1]
                    
    def nextFile(self):
        if self.files.index(self.actual) + 1 == len(self.files):
            return False
        else:
            self.actual = self.files[self.files.index(self.actual) + 1]
            return True
                
class CompilationEngine:
    def __init__(self, files):
        self.files = [w.replace(".jack", "T-test.xml") for w in files]
        self.actual = self.files[0]
        
    def compileClass(self):
        pass
        
    def compileClassVarDec(self):
        pass
    
    def compileSubroutine(self):
        pass
    
    def compileParameterList(self):
        pass
    
    def compilerVarDec(self):
        pass
        
    def compileStatements(self):
        pass
            
    def compileDo(self):
        pass
        
    def compileLet(self):
        pass
        
    def compileWhile(self):
        pass
        
    def compileReturn(self):
        pass
        
    def compileIf(self):
        pass
    
    def compileExpression(self):
        pass
        
    def compileTerm(self):
        pass
        
    def compileExpressionList(self):
        pass
  
if __name__ == "__main__":
    files = list()
    
    if os.path.isdir(sys.argv[1]):
        for file in os.listdir(sys.argv[1]):
            if file.endswith(".jack"):
                files.append(os.path.join(sys.argv[1], file))

    else:
        files.append(sys.argv[1])

        
    output = open("output.xml", "w")
    output.write("<tokens>\n")

    tokenizer = JackTokenizer(files)
    compiler = CompilationEngine(files)
    
    more_files = 1
    while more_files:
        if tokenizer.hasMoreTokens():
            type = tokenizer.tokenType()  
            
            if type == "KEYWORD":
                word = tokenizer.keyWord()
                
            elif type == "SYMBOL":
                word = tokenizer.symbol()
                
            elif type == "IDENTIFIER":
                word = tokenizer.identifier()
                
            elif type == "INT_CONST":
                word = tokenizer.intVal()
                
            elif type == "STRING_CONST":
                word = tokenizer.stringVal()
                    
            output.write("<%s> %s </%s>\n" % (type, word, type))
            
            tokenizer.advance()
        
        else:
            more_files = tokenizer.nextFile()
            if more_files:
                tokenizer.setFile()
    
    output.write("</tokens>")